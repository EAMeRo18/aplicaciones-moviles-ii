package com.example.noticiasuptal;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario>registrarUsuario(@Field("username")String correo, @Field("password")String password);

}
